package main

import (
	"fmt"
	"io"
	"net/http"
)

var mainTmpl = []byte(`
<html>
	<body>
	<form action="/login">
    <input type="submit" value="login" /></form>
	</body>
</html>
`)

func mainPage(w http.ResponseWriter, r *http.Request) {
	w.Write(mainTmpl)
}

var loginFormTmpl = []byte(`
<html>
	<body>
	<form action="/login" method="post">
		Login: <input type="text" name="login">
		Password: <input type="password" name="password">
		<input type="submit" value="Login">
	</form>
	</body>
</html>
`)

var auth = map[string]string{
	"alex": "pass",
}

func loginPage(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.Write(loginFormTmpl)
		return
	}

	r.ParseForm()

	pass, found := auth[r.FormValue("login")]

	if !found {
		w.WriteHeader(404)
		return
	}

	if pass != r.FormValue("password") {
		w.WriteHeader(400)
		return
	}

	io.WriteString(w, `{"token": "token-string"}`)
	w.WriteHeader(200)
}

func main() {
	http.HandleFunc("/", mainPage)
	http.HandleFunc("/login", loginPage)

	fmt.Println("starting server at :8080")
	http.ListenAndServe(":8080", nil)
}
